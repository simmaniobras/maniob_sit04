﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tablet/Casos")]
/// clase que alberga los elementos necesario para la creacion de un caso en al tablet  
public class clsDatosBaseParaLaTablet : ScriptableObject
{

    #region members
    [SerializeField]
    private float[] tramoEnMetros;
    [SerializeField]
    private float[] NumeroDeUsuarios;
    /*[SerializeField]
    private float contaneConductorDosSobre0;
    [SerializeField]
    private float contaneConductorCuatroSobre0;
    [SerializeField]
    private float contaneConductorDos;
    [SerializeField]
    private float contaneConductorCuatroe0;
    */
    [SerializeField]
    private float ConstanteNKVA_MAX_USUARIO;

   //[SerializeField]
    private float[] corrienteA;

    //[SerializeField]
    private float[] kvaTotales;

    //[SerializeField]
    private float[] momentoElectrico;

    //[SerializeField]
    private float[] parcial;

    [SerializeField]
    private float Voltaje_Fase_neutro_BT;


    private float[] tension_de_recibo;

    private float[] tension_de_envio;

    private float[] Regulacion_tension_acumulada;

    #endregion

    #region accesors

    /* public float _constaneConductorDosSobre0
     {
         get
         {
             return contaneConductorDosSobre0;
         }
     }

     public float _constaneConductorCuatroSobre0
     {
         get
         {
             return contaneConductorCuatroSobre0;
         }
     }

     public float _constaneConductorDos
     {
         get
         {
             return contaneConductorDos;
         }
     }

     public float _constaneConductorCuatroe0
     {
         get
         {
             return contaneConductorCuatroe0;
         }
     }*/

    public float[] _tramoEnMetros
    {
        get
        {
            return tramoEnMetros;
        }
    }

    public float[] _NumeroDeUsuarios
    {
        get
        {
            return NumeroDeUsuarios;
        }
    }

    public float[] _KvaTotalesTramo
    {
        get
        {
            return kvaTotales;
        }
    }


    public float[] _momentoElectrico
    {
        get
        {
            return momentoElectrico;
        }
    }

    public float[] _tension_de_recibo
    {
        get
        {
            return tension_de_recibo;
        }
    }


    public float[] _tension_de_envio
    {
        get
        {
            return tension_de_envio;
        }
    }

    public float[] _corrienteA
    {
        get
        {
            return corrienteA;
        }
    }

    public float[] _Regulacion_tension_acumulada
    {
        get
        {
            return Regulacion_tension_acumulada;
        }
    }
    


    #endregion
    #region monoBehaviour

    #endregion

    #region public methods


    /// <summary>
    /// genera todas las columnas de la tablet 
    /// </summary>
    /// <param name="contantesDeRegualcionDelCableElejido"></param>
    public void calcularDatosDetabla(float[] contantesDeRegualcionDelCableElejido)
    {
        calcularKvaTotalesTramo();
        calcularMomentoElectrico();
        calcularParcial(contantesDeRegualcionDelCableElejido);
        CalcularTension_de_recibo();
        clacularCorriente();
        calcularTencionAcumulada();
    }


    #endregion


    #region private methods

    private void calcularKvaTotalesTramo()
    {
       // Debug.Log("---------------------------------kvaTotales ----------------------------------------");
        float[] AuxVector = new float[NumeroDeUsuarios.Length];
        for (int i = 0; i < NumeroDeUsuarios.Length; i++)
        {
            AuxVector[i] = (NumeroDeUsuarios[i] * ConstanteNKVA_MAX_USUARIO) + 2f;
            
         //   Debug.Log(AuxVector[i]);
        }
        kvaTotales = AuxVector;
       //Debug.Log("-------------------------------------------------------------------------");

    }


    private void calcularMomentoElectrico()
    {
        //Debug.Log("-----------------------------calcularMomentoElectrico--------------------------------------------");
        float[] AuxVector = new float[tramoEnMetros.Length];
        for (int i = 0; i < tramoEnMetros.Length; i++)
        {
            AuxVector[i] = tramoEnMetros[i] * kvaTotales[i];
          //  Debug.Log(AuxVector[i]);
        }
        momentoElectrico = AuxVector;
        //Debug.Log("-------------------------------------------------------------------------");
    }


    private void calcularParcial(float[] ConstanteDelCableK)
    {
        //Debug.Log("-----------------------------calcularParcial-------------------------------------------");
        float[] AuxVector = new float[ConstanteDelCableK.Length];
        for (int i = 0; i < momentoElectrico.Length; i++)
        {
            AuxVector[i] = (ConstanteDelCableK[i]*momentoElectrico[i]) / 1000f;

          //  Debug.Log(ConstanteDelCableK[i]+"constante");
            //Debug.Log(AuxVector[i]);
        }
        parcial = AuxVector;

        //Debug.Log("-------------------------------------------------------------------------");
    }

    private void CalcularTension_de_recibo()
    {
        float[] AuxVector = new float[momentoElectrico.Length];
        float[] AuxVector2 = new float[momentoElectrico.Length];
        for (int i = 0; i <momentoElectrico.Length; i++)
        {
            if (i == 0)
            {
                AuxVector[i] = Voltaje_Fase_neutro_BT-((Voltaje_Fase_neutro_BT * parcial[i]) / 100f);
                AuxVector2[i] = Voltaje_Fase_neutro_BT;
            }
            else
            {
                AuxVector2[i] = AuxVector[i - 1];
                AuxVector[i]= AuxVector2[i] - ((AuxVector2[i]* parcial[i])/100f);

                if (i == 4)
                {
                    AuxVector[i] = AuxVector[2] - ((AuxVector[2] * parcial[i]) / 100f);
                    AuxVector2[i] = AuxVector[2];
                }

                if (i == 6)
                {
                    AuxVector2[i] = Voltaje_Fase_neutro_BT;
                    AuxVector[i] = AuxVector2[i]-((AuxVector2[i] * parcial[i]) / 100f);
                    
                }
                if (i == 9)
                {
                    AuxVector2[i] = AuxVector[6];
                    AuxVector[i] = AuxVector2[i] - ((AuxVector2[i] * parcial[i]) / 100f);
                    
                }
            }

        }

        tension_de_recibo = AuxVector;
        tension_de_envio = AuxVector2;
    }


    public void clacularCorriente()
    {
        float[] AuxVector = new float[tension_de_recibo.Length];
        for (int i = 0; i < momentoElectrico.Length; i++)
            AuxVector[i] = (kvaTotales[i] * 1000f)/ (3f*tension_de_recibo[i]);

        corrienteA = AuxVector;

    }

    public void calcularTencionAcumulada()
    {
        float aux = 0f;
        Regulacion_tension_acumulada = new float[tension_de_recibo.Length];

        for (int i = 0; i < tension_de_recibo.Length; i++)
        {
            if (i == 0)
            {
                aux=((tension_de_envio[i]-tension_de_recibo[i])*100f)/ tension_de_envio[i];
                Regulacion_tension_acumulada[i] = aux;
            }
            else
            {

                aux = (((tension_de_envio[i] - tension_de_recibo[i]) * 100f) / tension_de_envio[i])+ Regulacion_tension_acumulada[i-1];
                Regulacion_tension_acumulada[i] = aux;
                if (i == 4)
                {
                    aux = (((tension_de_envio[i] - tension_de_recibo[i]) * 100f) / tension_de_envio[i]) + Regulacion_tension_acumulada[2];
                    Regulacion_tension_acumulada[i] = aux;
                }
                if (i == 5)
                {
                    aux = (((tension_de_envio[i] - tension_de_recibo[i]) * 100f) / tension_de_envio[i]) + Regulacion_tension_acumulada[3];
                    Regulacion_tension_acumulada[i] = aux;

                }

                if (i == 6)
                {
                    aux = (((tension_de_envio[i] - tension_de_recibo[i]) * 100f) / tension_de_envio[i]);
                    Regulacion_tension_acumulada[i] = aux;

                }
                if (i == 10)
                {
                    aux = (((tension_de_envio[i] - tension_de_recibo[i]) * 100f) / tension_de_envio[i]) + Regulacion_tension_acumulada[6];
                    Regulacion_tension_acumulada[i] = aux;

                }
            }


            Debug.Log("regulacion tension acumulada#"+i+"= " + aux);
        }
    }
    
    #endregion


}
