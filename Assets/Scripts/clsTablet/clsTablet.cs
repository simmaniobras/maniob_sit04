﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using NSInterfaz;

namespace NSInterfazAvanzada
{
    public class clsTablet : AbstractSingletonPanelUIAnimation<PanelInterfazCalificacionFinal>
    {
        #region members
        [SerializeField]
        private clsRegistroDatosSituacion3 RefRegistrodedatosPractica3;

        [SerializeField]
        private clsDatosBaseParaLaTablet[] casosParaElEjercicio;

        [SerializeField]
        private clsDatosBaseParaLaTablet[] casosParaElEjercicioAux;

        [SerializeField]
        private TextMeshProUGUI[] LonguitudTramo;

        [SerializeField]
        private TextMeshProUGUI[] KVATotalesTramo;

        [SerializeField]
        private TextMeshProUGUI[] textsCorrienteA;

        [SerializeField]
        private TextMeshProUGUI[] TexttensionEnvio;

        [SerializeField]
        private TextMeshProUGUI[] TexttensionRecibo;

        [SerializeField]
        private TextMeshProUGUI[] TextCalibreFase;

        [SerializeField]
        private TMP_InputField[] inputsRegulacion;

        private clsDatosBaseParaLaTablet CasoActual;

        private clsDatosBaseParaLaTablet CasoActualSolucion;

        private Dictionary<string, float> ConstanteCalibres = new Dictionary<string, float>();

        private float[] calibresFaces;
        private float[] calibresAuxFaces;

        private float[] AleatorioCorrienteCables;
        private float factor;

        private int caso;

        public enum tiposDEconductor
        {
            cuatro,
            dos,
            cuatro_0,
            Dos_0
        };

        #endregion

        #region monoBehaviour
        private void Start()
        {
            factor = Random.Range(1.5f, 3.5f);
        }

        #endregion

        #region public methods

        public void revisarInput(int index)
        {
            float tmpinputReguación;
            /// correinte antes del cambio del dps
            float.TryParse(inputsRegulacion[index].text, out tmpinputReguación);

            if (tmpinputReguación > 5.0f)
            {
                inputsRegulacion[index].transform.GetChild(0).transform.GetChild(2).GetComponent<TextMeshProUGUI>().color = Color.red;   //transform.parent.GetComponent<Image>().enabled = true;
                //inputsRegulacion[index].transform.parent.GetComponent<Image>().enabled = true;
            }
            else
            {
                //inputsRegulacion[index].transform.parent.GetComponent<Image>().enabled = false;
                inputsRegulacion[index].transform.GetChild(0).transform.GetChild(2).GetComponent<TextMeshProUGUI>().color = Color.black;
            }
        }


        /// <summary>
        /// estafunsion actualiza al tablet y los valores de los puntos cuando el usuario elige conductor en los cables
        /// </summary>
        /// <param name="idDetramo">es el tramoa afectado (0= p0 a p4,1= p0 a p5,2= p0,p7 a p10,3= p0,p7 a p13 )</param>
        /// <param name="tipocable">es el tipod e condictor elegido (cuatro=4, dos=2, cuatro_0=4/0,dos_0=2/0 ) </param>
        public void asignarValoDeResistenciaTramo1(int idDeTramo, tiposDEconductor tipocable)
        {
            switch (idDeTramo)
            {
                case 0:
                    CambiarcablesTramo0(ValorResistenciaDelTipoDeCOnductor(tipocable));
                    break;
                case 1:
                    CambiarcablesTramo1(ValorResistenciaDelTipoDeCOnductor(tipocable));

                    break;
                case 2:
                    CambiarcablesTramo2(ValorResistenciaDelTipoDeCOnductor(tipocable));
                    break;
                case 3:
                    CambiarcablesTramo3(ValorResistenciaDelTipoDeCOnductor(tipocable));
                    break;
            }

            actualizarCaso();
        }

        /// <summary>
        /// Cambia el valor de resistencia del tramo 0
        /// </summary>
        /// <param name="argTipoCable">0 = 4, 1 = 2, 2 = 4/0, 3 = 2/0</param>
        public void AsignarValorResistenciaTramo0(int argTipoCable)
        {
            switch (argTipoCable)
            {

                case 0:
                    CambiarcablesTramo0(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.cuatro));
                    break;

                case 1:
                    CambiarcablesTramo0(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.dos));
                    break;

                case 2:
                    CambiarcablesTramo0(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.cuatro_0));
                    break;

                case 3:
                    CambiarcablesTramo0(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.Dos_0));
                    break;
            }
            actualizarCaso();
            CambiarAuxcablesTramo0(ConstanteCalibres["2/0"]);
        }

        /// <summary>
        /// Cambia el valor de resistencia del tramo 1
        /// </summary>
        /// <param name="argTipoCable">0 = 4, 1 = 2, 2 = 4/0, 3 = 2/0</param>
        public void AsignarValorResistenciaTramo1(int argTipoCable)
        {
            switch (argTipoCable)
            {

                case 0:
                    CambiarcablesTramo1(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.cuatro));
                    break;

                case 1:
                    CambiarcablesTramo1(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.dos));
                    break;

                case 2:
                    CambiarcablesTramo1(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.cuatro_0));
                    break;

                case 3:
                    CambiarcablesTramo1(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.Dos_0));
                    break;
            }
            actualizarCaso();
            CambiarAuxablesTramo1(ConstanteCalibres["2/0"]);
        }

        /// <summary>
        /// Cambia el valor de resistencia del tramo 2
        /// </summary>
        /// <param name="argTipoCable">0 = 4, 1 = 2, 2 = 4/0, 3 = 2/0</param>
        public void AsignarValorResistenciaTramo2(int argTipoCable)
        {
            switch (argTipoCable)
            {

                case 0:
                    CambiarcablesTramo2(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.cuatro));
                    break;

                case 1:
                    CambiarcablesTramo2(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.dos));
                    break;

                case 2:
                    CambiarcablesTramo2(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.cuatro_0));
                    break;

                case 3:
                    CambiarcablesTramo2(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.Dos_0));
                    break;
            }
            actualizarCaso();

        }

        /// <summary>
        /// Cambia el valor de resistencia del tramo 0
        /// </summary>
        /// <param name="argTipoCable">0 = 4, 1 = 2, 2 = 4/0, 3 = 2/0</param>
        public void AsignarValorResistenciaTramo3(int argTipoCable)
        {
            switch (argTipoCable)
            {

                case 0:
                    CambiarcablesTramo3(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.cuatro));
                    break;

                case 1:
                    CambiarcablesTramo3(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.dos));
                    break;

                case 2:
                    CambiarcablesTramo3(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.cuatro_0));
                    break;

                case 3:
                    CambiarcablesTramo3(ValorResistenciaDelTipoDeCOnductor(tiposDEconductor.Dos_0));
                    break;
            }
            actualizarCaso();

        }

        /// <summary>
        /// reinicia la tablet
        /// </summary>
        public void reiniciarCasoRandom()
        {
            generarCasoAleatorio();
        }

        /// <summary>
        /// retorn un array con  los 3 valores para los cables primeros, ya que el cable de mas abajo es el neutro
        /// </summary>
        /// <param name="idDeTramo"></param>
        /// <returns></returns>
        public float[] CorreinteDeCablesPorTramo(int idDeTramo)
        {
            float[] auxCorrienteCables = new float[3];
            var auxcorriente = CasoActual._corrienteA;
            switch (idDeTramo)
            {
                case 0:
                    var auxCOrriente = +auxcorriente[3] / 3f;
               
                    auxCorrienteCables[0] = auxCOrriente+ factor; 
                    auxCorrienteCables[1] = auxCOrriente; 
                    auxCorrienteCables[2] = auxCOrriente- factor; 

                    break;
                case 1:
                    var auxCOrriente2 = auxcorriente[5] / 3f;
                    auxCorrienteCables[0] = auxCOrriente2;
                    auxCorrienteCables[1] = auxCOrriente2+factor;
                    auxCorrienteCables[2] = auxCOrriente2-factor;
                    break;
                case 2:
                    var auxCOrriente3 = auxcorriente[8] / 3f;
                    auxCorrienteCables[0] =  auxCOrriente3;
                    auxCorrienteCables[1] =  auxCOrriente3-factor-0.1f;
                    auxCorrienteCables[2] =  auxCOrriente3+factor+0.1f;
                    break;
                case 3:
                    var auxCOrriente4 =  auxcorriente[11] / 3f;
                    auxCorrienteCables[0] =  auxCOrriente4- factor-0.5f;
                    auxCorrienteCables[1] = auxCOrriente4+ factor+0.5f;
                    auxCorrienteCables[2] =  auxCOrriente4;
                    break;
            }

            return auxCorrienteCables;
        }

        /// <summary>
        /// Esta tension se da cuando se mide una linea y un neutro  
        /// </summary>
        /// <param name="TramoAmedir">el tramo va de 1 a 4 dependeidno donde el este situado par amedir</param>
        public Vector3 tensioLineaAFase(int TramoAmedir)
        {
            var auxtTenionRecibo = CasoActual._tension_de_recibo;
            Vector3 aux = new Vector3(0, 0, 0);
            Debug.Log("este es el caso elejido"+TramoAmedir);
            Debug.Log("Este es la tension al promediar todo "+auxtTenionRecibo[3]);
            switch (TramoAmedir)
            {
                case 0:
                    aux = new Vector3(auxtTenionRecibo[3] + 3.02f, auxtTenionRecibo[3]- 3.02f, auxtTenionRecibo[2]);
                    break;
                case 1:
                    aux = new Vector3(auxtTenionRecibo[5] - factor, auxtTenionRecibo[5]+ factor, auxtTenionRecibo[5]);
                    break;
                case 2:
                    aux = new Vector3(auxtTenionRecibo[8]+ factor + 0.2f, auxtTenionRecibo[8] , auxtTenionRecibo[8] - factor - 0.2f);
                    break;
                case 3:
                    aux = new Vector3(auxtTenionRecibo[11], auxtTenionRecibo[11] + factor + 0.3f, auxtTenionRecibo[11] - factor - 0.3f);
                    break;

            }

            return aux;
        }

        /// <summary>
        /// cuando se ponen los cables entre linea 
        /// </summary>
        /// <param name="TramoAmedir"></param>
        /// <returns></returns>
        public Vector3 tensioLineaALinea(int TramoAmedir)
        {
            var auxtTenionRecibo = CasoActual._tension_de_recibo;
            Vector3 aux = new Vector3(0, 0, 0);
            Debug.Log("corroiente tramo---------------------->" + auxtTenionRecibo);
            Debug.Log("corroiente tramo---------------------->" + TramoAmedir);
            switch (TramoAmedir)
            {
                case 0:
                    aux = new Vector3((auxtTenionRecibo[3] + 3.02f) * Mathf.Sqrt(3), (auxtTenionRecibo[3] - 3.02f) * Mathf.Sqrt(3), (auxtTenionRecibo[2] * Mathf.Sqrt(3)));
                    Debug.Log("puntos de tramo 0" + auxtTenionRecibo[3] + " " + auxtTenionRecibo[3] + " " + auxtTenionRecibo[3] + " ");
                    break;
                case 1:
                    aux = new Vector3((auxtTenionRecibo[5] - factor) * Mathf.Sqrt(3), (auxtTenionRecibo[5] + factor) * Mathf.Sqrt(3), (auxtTenionRecibo[5]) * Mathf.Sqrt(3));
                    break;
                case 2:
                    aux = new Vector3((auxtTenionRecibo[8] + factor + 0.2f) * Mathf.Sqrt(3), (auxtTenionRecibo[8]) * Mathf.Sqrt(3), auxtTenionRecibo[8] - factor - 0.2f * Mathf.Sqrt(3));
                    break;
                case 3:
                    aux = new Vector3(auxtTenionRecibo[11] * Mathf.Sqrt(3), (auxtTenionRecibo[11] + factor + 0.3f) * Mathf.Sqrt(3), (auxtTenionRecibo[11] - factor - 0.3f) * Mathf.Sqrt(3));
                    break;

            }

            return aux;
        }

        public Vector3 Get_TensioTransformador()
        {
            return new Vector3(120.6662063F, 119.25288F, 121.07953F);
        }

        public Vector3 Get_CorrienteTransformador()
        {
            var corrientebase = SumatoriaCorrienteEncasoDiv3();
  
            Vector3 aux = new Vector3(0, 0, 0);
            switch (caso)
            {
                case 0:
                    aux = new Vector3(corrientebase-10f, corrientebase - 13f, corrientebase + 23f);
            
                    break;
                case 1:
                    aux = new Vector3(corrientebase - 9f, corrientebase +9f, corrientebase);
                    break;
                case 2:
                    aux = new Vector3(corrientebase - 14f, corrientebase + 17f, corrientebase -3f);
                    break;
            }
            return aux;
        } 

        private float SumatoriaCorrienteEncasoDiv3()
        {
            float sumatoria=0f;

            foreach (float dato in CasoActual._corrienteA)
            {
                Debug.Log("Corriente trefo ---> "+dato);
                sumatoria = dato + sumatoria;
            }

            return sumatoria/3f;
        }



        public void IniciarlizarValores()
        {
            ConstanteCalibres.Clear();
            ConstanteCalibres.Add("4", 2.383619f);
            ConstanteCalibres.Add("2", 1.443822f);
            ConstanteCalibres.Add("2/0", 0.721945f);
            ConstanteCalibres.Add("4/0", 0.459f);
            generarCasoAleatorio();
        }

        /// <summary>
        /// funcion que retorna el tipo de cabler actual en el tramo
        /// </summary>
        /// <param name="idTramo">id de tramo a revizar </param>
        /// <returns></returns>
        public int GetCableSeleccionadoTramo(int idTramo)
        {
            int auxNum = 0;
            switch (idTramo)
            {
                case 0:
                    auxNum=convertirCableACodigoTramo(calibresFaces[3]);
                    break;
                case 1:
                    auxNum= convertirCableACodigoTramo(calibresFaces[5]);
                    break;
                case 2:
                    auxNum=convertirCableACodigoTramo(calibresFaces[9]);
                    break;
                case 3:
                    auxNum=convertirCableACodigoTramo(calibresFaces[12]);
                    break;
            }
            return auxNum;
        }


        #endregion

        #region private methods

        /// <summary>
        /// Elije el valor y el caso a utlizar
        /// </summary>
        private void generarCasoAleatorio()
        {
            AleatorioCorrienteCables = new float[3];
            caso = 0;// Random.Range(0, 3);
            Debug.Log("caso elegido para esta practica "+caso);
            AleatorioCorrienteCables[0] = Random.Range(0f, 0.05f);
            AleatorioCorrienteCables[1] = Random.Range(0f, 0.05f);
            AleatorioCorrienteCables[2] = Random.Range(0f, 0.05f);

            generarvectorDeCalibres();
            generarvectorDeCalibresAux();
            CasoActual = casosParaElEjercicio[caso];
            CasoActual.calcularDatosDetabla(calibresFaces);
            casosParaElEjercicioAux[caso].calcularDatosDetabla(calibresAuxFaces);
            actualizartaDatosTablet();
            ResuladoFInalAux();
            Mandarvaloresalregistro();

        }

        private void ResuladoFInalAux()
        {
            switch (caso)
            {
                case 0:
                    CambiarAuxcablesTramo0(ConstanteCalibres["2/0"]);
                    break;
                case 1:
                    CambiarAuxcablesTramo2(ConstanteCalibres["2/0"]);
                    break;
                case 2:
                    CambiarAuxcablesTramo3(ConstanteCalibres["2/0"]);
                    break;
            }
        }


        private void actualizarCaso()
        {
            CasoActual = casosParaElEjercicio[caso];
            CasoActual.calcularDatosDetabla(calibresFaces);
            casosParaElEjercicioAux[caso].calcularDatosDetabla(calibresAuxFaces);
            actualizartaDatosTablet();

            Debug.Log("ya actualize los datos");
        }

        private void generarvectorDeCalibres()
        {
            calibresFaces = new float[13];
            for (int i = 0; i < 13; i++)
            {
                if (caso == 0)
                {
                    calibresFaces[i] = ConstanteCalibres["4"];
                    // Debug.Log(calibresFaces[i] + "contante generada en la tablet");
                }
                if (caso == 1)
                {
                    calibresFaces[i] = ConstanteCalibres["2"];
                }
                if (caso == 2)
                {

                    if(i<10)
                        calibresFaces[i] = ConstanteCalibres["2"];
                    else
                        calibresFaces[i] = ConstanteCalibres["4"];
                }

            }
        }


        private void generarvectorDeCalibresAux()
        {
            calibresAuxFaces = new float[13];
            for (int i = 0; i < 13; i++)
            {
                if (caso == 0)
                {
                    calibresAuxFaces[i] = ConstanteCalibres["4"];
                    // Debug.Log(calibresFaces[i] + "contante generada en la tablet");
                }
                if (caso == 1)
                {
                    calibresFaces[i] = ConstanteCalibres["2"];
                }
                if (caso == 2)
                {

                    if (i < 10)
                        calibresFaces[i] = ConstanteCalibres["2"];
                    else
                        calibresFaces[i] = ConstanteCalibres["4"];
                }
            }
        }



        private void actualizartaDatosTablet()
        {
            var auxTramo = CasoActual._tramoEnMetros;
            var auxKvaTotales = CasoActual._KvaTotalesTramo;
            var auxcorriente = CasoActual._corrienteA;
            var auxtensionEnvio = CasoActual._tension_de_envio;
            var auxtTenionRecibo = CasoActual._tension_de_recibo;

            for (int i = 0; i < 13; i++)
            {
                LonguitudTramo[i].text = auxTramo[i].ToString("0.00");
                KVATotalesTramo[i].text = auxKvaTotales[i].ToString("0.00");
                textsCorrienteA[i].text = auxcorriente[i].ToString("0.00");
                TexttensionEnvio[i].text = auxtensionEnvio[i].ToString("0.00");
                TexttensionRecibo[i].text = auxtTenionRecibo[i].ToString("0.00");
                TextCalibreFase[i].text = ConvertirConstanteEnValorKey(calibresFaces[i]);

            }

        }

        private string ConvertirConstanteEnValorKey(float valor)
        {
            var aux = "";

            if (ConstanteCalibres["4"] == valor)
                aux = "4";

            if (ConstanteCalibres["4/0"] == valor)
                aux = "4";
            if (ConstanteCalibres["2"] == valor)
                aux = "2";

            if (ConstanteCalibres["2/0"] == valor)
                aux = "2/0";

            return aux;
        }

        /// <summary>
        /// resive la resistensia del tramo y retorna la resistensia 
        /// </summary>
        /// <returns></returns>
        private int convertirCableACodigoTramo(float num)
        {
            int aux = 0;
            if (num == 2.383619f)
                aux = 0;
            if (num == 1.443822f)
                aux = 1;
            if (num == 0.721945f)
                aux = 2;
            if (num == 0.459f)
                aux = 3;

            Debug.Log(aux);

            return aux;

        }


        //----------------------------------------------------------------------------

        private void CambiarcablesTramo0(float ValorCable)
        {

            calibresFaces[0] = ValorCable;
            calibresFaces[1] = ValorCable;
            calibresFaces[2] = ValorCable;
            calibresFaces[3] = ValorCable;
        }
        
        private void CambiarcablesTramo1(float ValorCable)
        {
            calibresFaces[0] = ValorCable;
            calibresFaces[1] = ValorCable;
            calibresFaces[2] = ValorCable;
            calibresFaces[4] = ValorCable;
            calibresFaces[5] = ValorCable;
        }

        private void CambiarcablesTramo2(float ValorCable)
        {
            calibresFaces[0] = ValorCable;
            calibresFaces[6] = ValorCable;
            calibresFaces[7] = ValorCable;
            calibresFaces[8] = ValorCable;
            
        }

        private void CambiarcablesTramo3(float ValorCable)
        {
            calibresFaces[0] = ValorCable;
            calibresFaces[6] = ValorCable;
            calibresFaces[9] = ValorCable;
            calibresFaces[10] = ValorCable;
            calibresFaces[11] = ValorCable;
        }


        //----------------------------------------------------------------------------

        private void CambiarAuxcablesTramo0(float ValorCable)
        {

            calibresAuxFaces[0] = ValorCable;
            calibresAuxFaces[1] = ValorCable;
            calibresAuxFaces[2] = ValorCable;
            calibresAuxFaces[3] = ValorCable;
            casosParaElEjercicioAux[caso].calcularDatosDetabla(calibresAuxFaces);
           // MandarvaloresalregistroCalculoFinal();
        }
        
        private void CambiarAuxablesTramo1(float ValorCable)
        {
            calibresAuxFaces[0] = ValorCable;
            calibresAuxFaces[1] = ValorCable;
            calibresAuxFaces[2] = ValorCable;
            calibresAuxFaces[4] = ValorCable;
            calibresAuxFaces[5] = ValorCable;
            casosParaElEjercicioAux[caso].calcularDatosDetabla(calibresAuxFaces);
           // MandarvaloresalregistroCalculoFinal();
        }

        private void CambiarAuxcablesTramo2(float ValorCable)
        {
            calibresAuxFaces[6] = ValorCable;
            calibresAuxFaces[7] = ValorCable;
            calibresAuxFaces[8] = ValorCable;
            calibresAuxFaces[9] = ValorCable;
            casosParaElEjercicioAux[caso].calcularDatosDetabla(calibresAuxFaces);
           //MandarvaloresalregistroCalculoFinal();
        }

        private void CambiarAuxcablesTramo3(float ValorCable)
        {
            calibresAuxFaces[10] = ValorCable;
            calibresAuxFaces[11] = ValorCable;
            calibresAuxFaces[12] = ValorCable;
            casosParaElEjercicioAux[caso].calcularDatosDetabla(calibresAuxFaces);
          // MandarvaloresalregistroCalculoFinal();
        }

        //---------------------------------------------------------------------------

        private float ValorResistenciaDelTipoDeCOnductor(tiposDEconductor tipo)
        {
            float aux = 0f;
            switch (tipo)
            {
                case tiposDEconductor.cuatro:
                    aux = ConstanteCalibres["4"];
                    break;
                case tiposDEconductor.cuatro_0:
                    aux = ConstanteCalibres["4/0"];
                    break;
                case tiposDEconductor.dos:
                    aux = ConstanteCalibres["2"];
                    break;
                case tiposDEconductor.Dos_0:
                    aux = ConstanteCalibres["2/0"];
                    break;
            }
            return aux;
        }

        private void Mandarvaloresalregistro()
        {
            RefRegistrodedatosPractica3.SetDatosTransformadorCorriente(Get_CorrienteTransformador().x, Get_CorrienteTransformador().y, Get_CorrienteTransformador().z);
            RefRegistrodedatosPractica3.SetDatosTransformadorTension(Get_TensioTransformador().x, Get_TensioTransformador().y, Get_TensioTransformador().z);
            RefRegistrodedatosPractica3.SetDatosCorrienteAntes(CasoActual._corrienteA[3], CasoActual._corrienteA[5], CasoActual._corrienteA[9], CasoActual._corrienteA[12]);
            RefRegistrodedatosPractica3.SetDatosTensionAntes(CasoActual._tension_de_recibo[3], CasoActual._tension_de_recibo[5], CasoActual._tension_de_recibo[9], CasoActual._tension_de_recibo[12]);
            RefRegistrodedatosPractica3.SetDatosCorrienteDespues(casosParaElEjercicioAux[caso]._corrienteA[3], casosParaElEjercicioAux[caso]._corrienteA[5], casosParaElEjercicioAux[caso]._corrienteA[9], casosParaElEjercicioAux[caso]._corrienteA[12]); 
            RefRegistrodedatosPractica3.SetDatosTensionDespues(casosParaElEjercicioAux[caso]._tension_de_recibo[3], casosParaElEjercicioAux[caso]._tension_de_recibo[5], casosParaElEjercicioAux[caso]._tension_de_recibo[9], casosParaElEjercicioAux[caso]._tension_de_recibo[12]);
            RefRegistrodedatosPractica3.SetDatosinputRegulacion(casosParaElEjercicioAux[caso]._Regulacion_tension_acumulada[3],casosParaElEjercicioAux[caso]._Regulacion_tension_acumulada[5], casosParaElEjercicioAux[caso]._Regulacion_tension_acumulada[9], casosParaElEjercicioAux[caso]._Regulacion_tension_acumulada[12]);

        }

       /* private void MandarvaloresalregistroCalculoFinal()
        {
            RefRegistrodedatosPractica3.SetDatosCorrienteDespues(casosParaElEjercicioAux[caso]._corrienteA[3], casosParaElEjercicioAux[caso]._corrienteA[5], casosParaElEjercicioAux[caso]._corrienteA[9], casosParaElEjercicioAux[caso]._corrienteA[12]);
            RefRegistrodedatosPractica3.SetDatosTensionDespues(casosParaElEjercicioAux[caso]._tension_de_recibo[3], casosParaElEjercicioAux[caso]._tension_de_recibo[5], casosParaElEjercicioAux[caso]._tension_de_recibo[9], casosParaElEjercicioAux[caso]._tension_de_recibo[12]);
            RefRegistrodedatosPractica3.SetDatosinputRegulacion(casosParaElEjercicioAux[caso]._Regulacion_tension_acumulada[3], casosParaElEjercicioAux[caso]._Regulacion_tension_acumulada[5], casosParaElEjercicioAux[caso]._Regulacion_tension_acumulada[9], casosParaElEjercicioAux[caso]._Regulacion_tension_acumulada[12]);
        }*/


        #endregion
    }
}
