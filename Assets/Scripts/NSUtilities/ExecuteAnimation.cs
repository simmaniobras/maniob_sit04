﻿#pragma warning disable 0649
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace NSUtilities
{
    [RequireComponent(typeof(Animator))]
    public class ExecuteAnimation : MonoBehaviour
    {
        public UnityEvent OnAnimationEnd;

        private Animator refAnimator;
        
        private void Awake()
        {
            refAnimator = GetComponent<Animator>();
        }

        public void PlayAnimacion(string argAnimationName, UnityAction argOnAnimationFinish = null, float argDelay = 0)
        {
            gameObject.SetActive(true);

            if (argOnAnimationFinish != null)
                StartCoroutine(CouPlayAnimation(argAnimationName, argOnAnimationFinish, argDelay));
        }
        
        public void TriggerAnimationEnd()
        {
            OnAnimationEnd.Invoke();
        }

        private IEnumerator CouPlayAnimation(string argAnimationName, UnityAction argOnAnimationFinish, float argDelay)
        {
            refAnimator.Play(argAnimationName);
            yield return null;
            Debug.Log("Lengt Animation : " + refAnimator.GetCurrentAnimatorStateInfo(0).length);
            yield return new WaitForSeconds(refAnimator.GetCurrentAnimatorStateInfo(0).length);
            yield return new WaitForSeconds(argDelay);
            TriggerAnimationEnd();
            argOnAnimationFinish();
            gameObject.SetActive(false);
        }
    } 
}
