﻿using UnityEngine;
using UnityEngine.Events;

namespace NSSituacion2
{
    public class ControladorDPSMaloRamdom : MonoBehaviour
    {
        #region members

        [SerializeField]
        private GameObject[] DPSBueno;

        [SerializeField]
        private GameObject[] DPSMalo;

        [SerializeField]
        private GameObject[] DPSBuenoAfuera;

        [SerializeField]
        private GameObject[] DPSMaloAfuera;

        [SerializeField]
        private GameObject[] DPSBuenoPrevisualizacion;

        [SerializeField]
        private GameObject[] DPSMaloPrevisualizacion;
        #endregion

        #region Events

        [Header("Events")]
        [Space(10)]
        public UnityEvent OnAllDPSRemplaced;
        #endregion

        private void Awake()
        {
            SetRamdomDPSBad();
        }

        #region methods

        private void SetRamdomDPSBad()
        {
            var tmpDPSIsBad = Random.Range(0f, 1f);
            var tmpIndexDPSBad = 0;

            if (tmpDPSIsBad <= 0.33f)
                tmpIndexDPSBad = 0;
            else if (tmpDPSIsBad <= 0.66f)
                tmpIndexDPSBad = 1;
            else if (tmpDPSIsBad <= 1f)
                tmpIndexDPSBad = 2;

            for (int i = 0; i < DPSBueno.Length; i++)
            {
                if (i == tmpIndexDPSBad)
                {
                    DPSBueno[i].SetActive(false);
                    DPSMalo[i].SetActive(true);

                    DPSBuenoAfuera[i].SetActive(false);
                    DPSMaloAfuera[i].SetActive(true);

                    DPSBuenoPrevisualizacion[i].SetActive(false);
                    DPSMaloPrevisualizacion[i].SetActive(true);
                }
                else
                {
                    DPSBueno[i].SetActive(true);
                    DPSMalo[i].SetActive(false);

                    DPSBuenoAfuera[i].SetActive(true);
                    DPSMaloAfuera[i].SetActive(false);

                    DPSBuenoPrevisualizacion[i].SetActive(true);
                    DPSMaloPrevisualizacion[i].SetActive(false);
                }
            }
        }

        public void SetDPSGood(int argIndexDPS)
        {
            DPSBueno[argIndexDPS].SetActive(true);
            DPSMalo[argIndexDPS].SetActive(false);

            DPSBuenoAfuera[argIndexDPS].SetActive(true);
            DPSMaloAfuera[argIndexDPS].SetActive(false);

            DPSBuenoPrevisualizacion[argIndexDPS].SetActive(true);
            DPSMaloPrevisualizacion[argIndexDPS].SetActive(false);

            foreach (var tmpDPS in DPSBueno)
                if (!tmpDPS.activeSelf)
                    return;

            OnAllDPSRemplaced.Invoke();
        }
        #endregion
    }
}