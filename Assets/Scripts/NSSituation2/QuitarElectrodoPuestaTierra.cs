﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSituacion2
{
    public class QuitarElectrodoPuestaTierra : MonoBehaviour
    {
        #region members

        private bool sePuedeQuitarElectrodo;
        #endregion

        #region accesors

        public bool _sePuedeQuitarElectrodo
        {
            set
            {
                sePuedeQuitarElectrodo = value;
            }
        }
        #endregion

        #region public methods

        public void OnButtonSePuedeQuitarElectrodo()
        {
            if (sePuedeQuitarElectrodo)
            {
                sePuedeQuitarElectrodo = false;
                gameObject.SetActive(false);
            }
        }
        #endregion
    }
}