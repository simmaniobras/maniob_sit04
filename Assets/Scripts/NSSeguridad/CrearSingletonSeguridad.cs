﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSeguridad
{
    public class CrearSingletonSeguridad : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {
            ClsSeguridad.CreateInstance();
        }
    }
}