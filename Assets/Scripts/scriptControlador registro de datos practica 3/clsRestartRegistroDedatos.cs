﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class clsRestartRegistroDedatos : MonoBehaviour {

    public Toggle Trasformador;
    public Toggle TramosFinales;
    public GameObject objTransformador;
    public GameObject objTramosFinales;

    public GameObject RegistroDedatos;
    public RectTransform imagenRegistroDeDaTos;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void Reiniciar()
    {
        Trasformador.isOn = true;
        TramosFinales.isOn = false;
        objTramosFinales.SetActive(false);
        objTransformador.SetActive(true);
    }

    public void agregarALpdf()
    {
        Trasformador.isOn = false;
        TramosFinales.isOn = true;
        objTramosFinales.SetActive(true);
        objTransformador.SetActive(false);
        Debug.Log("ya inserte el el registo de datos");
        var registro2= Instantiate(RegistroDedatos, imagenRegistroDeDaTos);
        registro2.gameObject.transform.localPosition =new Vector3(0f, -90f,0f); 
        Reiniciar();

    }


}
