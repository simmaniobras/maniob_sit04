﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSCelular
{
    public class SetMessage : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI textMensaje;

        public void SetMensaje(string argMensaje)
        {
            textMensaje.text = argMensaje;
            LayoutRebuilder.ForceRebuildLayoutImmediate(gameObject.GetComponent<RectTransform>());
        }
    }
}