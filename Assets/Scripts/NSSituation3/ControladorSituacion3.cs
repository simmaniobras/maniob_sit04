﻿using NSActiveZones;
using NSBoxMessage;
using NSInterfaz;
using NSInterfazAvanzada;
using NSPinzaVoltaje;
using System;
using System.Collections;
using NSSituacion2;
using NSSituacionGeneral;
using NSTraduccionIdiomas;
using NSUtilities;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace NSSituacion3
{
    public class ControladorSituacion3 : AbstractControladorSituacion
    {
        #region members

        private bool zonaSeguridadColocada;

        private bool avatarVestido;

        private bool implementosAlturaColocados;

        [SerializeField] private ActiveZonesController refActiveZonesController;

        [SerializeField] private clsRegistroDatosSituacion3 refRegistroDatosSituacion2;

        [SerializeField] private ZonaActual zonaActual = ZonaActual.Afuera;

        [SerializeField, Header("Valores transformador")]
        private Transformador[] arrayTransformadores;

        [SerializeField] private Image imageTransformador;

        [SerializeField] private clsTablet refTablet;

        private int indexTransformadorSeleccionado;

        private float[] corrienteCablesTransformador = new float[3];

        private float[] corrienteCablesMacromedidor = new float[3];

        private float[] tensionLineaPuntasPinza = new float[3];

        private float[] tensionFasePuntasPinza = new float[3];

        private float[] potenciaMacroMedidor = new float[3];

        private float potenciaTransformadorActual;

        private int tramoSenializado;

        private int tramoSeleccionado = -1;

        [SerializeField] private GameObject puntosSeleccionCable;

        [SerializeField] private ControladorPerdigaConPinzas3 refControladorPerdigaConPinzas3;

        [Header("Dependencias UI interfaz")] [SerializeField]
        private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionVestimentaSeguridad;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionSenializacionSeguridad;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionHerramientasAlturas;

        [SerializeField] private CanvasGroup canvasGroupBotonesDerecha;

        [SerializeField] private CanvasGroup canvasGroupBotonesIzquierda;

        #endregion

        #region Herramientas

        [Header("Herramientas"), Space(10)] [Header("Descolgar corta circuitos"), Space(10)] [SerializeField]
        private GameObject pertigaDescolgarFusibles;

        [SerializeField] private GameObject pertigaMensajeNoOperar;

        [SerializeField] private GameObject letreroNoOperar;

        [SerializeField] private GameObject pertigaQuitarLetrero;

        [SerializeField] private GameObject pertigaColgarFusibles;

        [Header("Puesta tierra"), Space(10)] [SerializeField]
        private GameObject pertigaPinzas;

        [SerializeField] private GameObject electrodoPuestaTierra;

        [SerializeField] private GameObject quitarPinzasZonaActiva;
        
        [SerializeField] private GameObject ponerPinzasZonaActiva;

        [Header("Herramientas recurrentes"), Space(10)] [SerializeField]
        private GameObject puntaDetectoraTension;

        [SerializeField] private GameObject pinzaAmperimetro;

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectTogglesHerramientasAlturas;

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectTogglesVestimentaSeguridad;

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectToggleSenializacionSeguridad;

        /// <summary>
        /// para saber en que etapa del corta circuito se va en el uso de herramientas
        /// </summary>
        private int etapaHerramientasCortaCircuitos;

        [SerializeField] private Button buttonVestirAvatar;

        #endregion

        #region animaciones

        [Header("Animators"), Space(10)] [SerializeField]
        private Animator animatorCortaCircuitos;

        [Header("Animators controller"), Space(10)] [SerializeField]
        private RuntimeAnimatorController animatorControllerDescolgarCortaCircuitos;

        private int countTriggerAnimationDescolgarCortaCircuitos;

        [SerializeField] private RuntimeAnimatorController animatorControllerColgarCortaCircuitos;

        private int countTriggerAnimationColgarCortaCircuitos;

        #endregion

        #region zonas activas

        [Header("Zonas Activas")] [SerializeField]
        private GameObject zonasActivasDescolgarFusibles;

        [SerializeField] private GameObject zonasActivasColgarFusibles;

        [SerializeField] private GameObject zonaActivaLetreroNoOperar;

        [SerializeField] private GameObject zonaActivaQuitarLetreroNoOperar;

        #endregion

        #region cortaCircuitos

        [Header("Senializacion tramos")] [SerializeField]
        private GameObject[] arrayTramosSenializacion;

        [Header("Cambio cables tramos")] [SerializeField]
        private GameObject[] arrayTramosCambioCables;

        #endregion

        #region controladoresTension

        [Header("Controladores tension")] [SerializeField]
        private ControladorTensionTransformador refControladorTensionTransformador;

        [SerializeField] private ControladorTensionCortaCircuitos refControladorTensionCortaCircuitos;

        #endregion

        #region rompeCargas

        [Header("Rompe Cargas")] [SerializeField]
        private PanelInterfazSoloAnimacion refPanelAnimationRompeCargasMostrar;

        [SerializeField] private ExecuteAnimation refExecuteAnimationRompeCargas;

        #endregion

        #region Tablet

        #endregion

        #region accesores

        public int _indexTransformadorSeleccionado
        {
            get { return indexTransformadorSeleccionado; }
        }

        public bool _zonaSeguridadColocada
        {
            set
            {
                zonaSeguridadColocada = value;
                SetEtapaZonaSeguridad();
            }
            get { return zonaSeguridadColocada; }
        }

        public bool _avatarVestido
        {
            set
            {
                avatarVestido = value;
                SetEtapaVestirAvatar();
            }
            get { return avatarVestido; }
        }

        public float[] _corrienteCablesTransformador
        {
            get { return corrienteCablesTransformador; }
        }

        public float[] _corrienteCablesMacromedidor
        {
            get { return corrienteCablesMacromedidor; }
        }

        public float[] _tensionLineaPuntasPinza
        {
            get { return tensionLineaPuntasPinza; }
        }

        public float[] _tensionFasePuntasPinza
        {
            get { return tensionFasePuntasPinza; }
        }

        public ZonaActual _etapaSituacionActual
        {
            get { return zonaActual; }
        }

        public float _potenciaTransformadorActual
        {
            get { return potenciaTransformadorActual; }
        }

        public int _etapaHerramientasCortaCircuitos
        {
            set { etapaHerramientasCortaCircuitos = value; }
        }

        public int TramoSeleccionado
        {
            get { return tramoSeleccionado; }
            set { tramoSeleccionado = value; }
        }

        #endregion

        private void Awake()
        {
            refTablet.IniciarlizarValores();
            SetTransformadorRamdom();
          

            refActiveZonesController.InteractuableZone("TransformadorPertiga", false);
            refActiveZonesController.InteractuableZone("MedicionTension", false);
            refActiveZonesController.InteractuableZone("TransformadorPinza", false);
            refActiveZonesController.InteractuableZone("ZonasSeguridad", false);
            refActiveZonesController.InteractuableZone("ZonasSeguridadInicial", false);
            refActiveZonesController.InteractuableZone("CambiarCable", false);
        }

        private void OnEnable()
        {
            CalcularValoresSistema();
        }
        #region private methods

        private void OnButtonAceptarMensajeInformacion()
        {
            refActiveZonesController.ActiveAllZones();
        }

        private void SetTransformadorRamdom()
        {
            indexTransformadorSeleccionado = Random.Range(0, arrayTransformadores.Length);
            imageTransformador.sprite = arrayTransformadores[indexTransformadorSeleccionado].spriteTransformador;
        }

        public void ActivarPanelTramoCambioCables()
        {
            if (tramoSeleccionado == -1)
                return;

            for (int i = 0; i < arrayTramosCambioCables.Length; i++)
            {
                GameObject tmpTramoCambioCable;

                if (i == tramoSeleccionado)
                {
                    tmpTramoCambioCable = arrayTramosCambioCables[i];
                    tmpTramoCambioCable.SetActive(true);
                    tmpTramoCambioCable.GetComponent<PanelCablesTramo>().ActivarToggle(refTablet.GetCableSeleccionadoTramo(tramoSeleccionado));
                }
            }
        }

        public void DesactivarPanelTramoCambioCables()
        {
            foreach (var tmpPanelCambioCable in arrayTramosCambioCables)
                tmpPanelCambioCable.SetActive(false);
        }

        /// <summary>
        /// Calcula todos los valores del sistema al azar, en el awake de este script y luego en el evento de cuando se cambian todos los dps
        /// </summary>
        public void CalcularValoresSistema()
        {
            potenciaTransformadorActual = (arrayTransformadores[indexTransformadorSeleccionado].potenciaKVA * 1000f);

            var tmpCorrienteTransformador = refTablet.Get_CorrienteTransformador();

            corrienteCablesTransformador[0] = tmpCorrienteTransformador[0];
            corrienteCablesTransformador[1] = tmpCorrienteTransformador[1];
            corrienteCablesTransformador[2] = tmpCorrienteTransformador[2];

            var tamTensionLineaFace = refTablet.Get_TensioTransformador();
            ;

            tensionFasePuntasPinza[0] = tamTensionLineaFace.x;
            tensionFasePuntasPinza[1] = tamTensionLineaFace.y;
            tensionFasePuntasPinza[2] = tamTensionLineaFace.z;


            for (int i = 0; i < 3; i++)
            {
                //corrienteCablesMacromedidor[i] = corrienteCablesTransformador[i] / 40; //gran variacion del 0.02%
                tensionLineaPuntasPinza[i] = tensionFasePuntasPinza[i] * 1.732050f; // potenciaTransformadorActual/(1.732050f * corrienteCablesTransformador[i]);
                //tensionFasePuntasPinza[i] = tensionLineaPuntasPinza[i] / 1.732050f;
                //potenciaMacroMedidor[i] = 1.732050f * tensionLineaPuntasPinza[i] * corrienteCablesMacromedidor[i] * 0.95f;
            }
        }

        #endregion

        #region public methods

        public void OnButtonZonaActivaTransformador()
        {
            Debug.Log("OnButtonZonaActivaTransformador");

            if (avatarVestido)
            {
                if (zonaSeguridadColocada)
                {
                    refActiveZonesController.DesactiveAllZones();
                    PanelInterfazTransformador._instance.Mostrar();
                    SetEtapaTransformador();
                }
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonZonaActivaTransformadorPertiga()
        {
            refActiveZonesController.InteractuableZone("TransformadorPertiga", false);
            refActiveZonesController.DesactivateZone("TransformadorPertiga");

            if (etapaHerramientasCortaCircuitos == 0)
            {
                pertigaDescolgarFusibles.SetActive(true);
                zonasActivasDescolgarFusibles.SetActive(true);
                countTriggerAnimationColgarCortaCircuitos = 0;
                countTriggerAnimationDescolgarCortaCircuitos = 0;

                for (int i = 0; i < zonasActivasDescolgarFusibles.transform.childCount; i++)
                    zonasActivasDescolgarFusibles.transform.GetChild(i).gameObject.SetActive(true);

                for (int i = 0; i < zonasActivasColgarFusibles.transform.childCount; i++)
                    zonasActivasColgarFusibles.transform.GetChild(i).gameObject.SetActive(true);
            }

            PanelInterfazCortaCircuitos._instance.Mostrar();
        }

        public void OnButtonZonaActivaTransformadorPuntaTension()
        {
            puntaDetectoraTension.SetActive(true);
            refActiveZonesController.InteractuableZone("MedicionTension", false);
            refActiveZonesController.DesactivateZone("MedicionTension");
            PanelInterfazTransformador._instance.Mostrar();
        }

        public void OnButtonZonaActivaTransformadorPinza()
        {
            pinzaAmperimetro.SetActive(true);
            pinzaAmperimetro.GetComponent<PinzaVoltaje>().RotarPinza(0);
            refActiveZonesController.InteractuableZone("TransformadorPinza", false);
            refActiveZonesController.DesactivateZone("TransformadorPinza");
            PanelInterfazTransformador._instance.Mostrar();
        }

        public void OnButtonZonaActivaPuestaTierra()
        {
            if (!cortaCircuitosAbiertos)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeCortaDesconectarCircuitos"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                return;
            }

            refActiveZonesController.InteractuableZone("PuestaTierra", false);
            refActiveZonesController.DesactivateZone("PuestaTierra");
            PanelInterfazPuestaTierra._instance.Mostrar();
        }

        /// <summary>
        /// Selecciona el tramo con el indice indicado para medir tension
        /// </summary>
        /// <param name="argIndexTramo">Indice del tramo seleccionado</param>
        public void OnButtonZonaActivaTramoMedicionTension(int argIndexTramo)
        {
            PanelInterfazCables._instance.Mostrar();
            puntaDetectoraTension.SetActive(true);

            if (!cortaCircuitosAbiertos)
                PanelInterfazCables._instance.SetTensionCables(true, true, true, true);
            else
                PanelInterfazCables._instance.SetTensionCables(false, false, false, false);

            refActiveZonesController.InteractuableZone("MedicionTension", false);
            refActiveZonesController.DesactivateZone("MedicionTension");
        }

        /// <summary>
        /// señaliza el tramo con el indice indicado
        /// </summary>
        /// <param name="argIndexTramo"></param>
        public void OnButtonZonaActivaTramoSenializacionSeguridad(int argIndexTramo)
        {
            tramoSenializado = argIndexTramo;
            ActivarTramoSenializacion(argIndexTramo);
            refActiveZonesController.DesactivateZone("ZonasSeguridad");
            refActiveZonesController.InteractuableZone("ZonasSeguridad", false);
            canvasGroupBotonesDerecha.interactable = true;
            canvasGroupBotonesIzquierda.interactable = true;
        }

        /// <summary>
        /// señaliza el tramo con el indice indicado
        /// </summary>
        /// <param name="argIndexTramo"></param>
        public void OnButtonZonaActivaTramoPinza(int argIndexTramo)
        {
            tramoSeleccionado = argIndexTramo;
            PanelInterfazCables._instance.Mostrar();
            pinzaAmperimetro.SetActive(true);
            pinzaAmperimetro.GetComponent<PinzaVoltaje>().RotarPinza(90f);
            refActiveZonesController.DesactivateZone("TransformadorPinza");
            refActiveZonesController.InteractuableZone("TransformadorPinza", false);
        }

        /// <summary>
        /// muestra los cables del tramo seleccionado
        /// </summary>
        /// <param name="argIndexTramo"></param>
        public void OnButtonZonaActivaTramoCables(int argIndexTramo)
        {
            if (refControladorPerdigaConPinzas3.PuestaTierraColocada)
            {
                tramoSeleccionado = argIndexTramo;
                PanelInterfazCables._instance.Mostrar();
                refActiveZonesController.DesactivateZone("CambiarCable");
                refActiveZonesController.InteractuableZone("CambiarCable", false);
                puntosSeleccionCable.SetActive(true);
                ActivarPanelTramoCambioCables();
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajePuestaTierraNoColocada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
        }

        public void OnButtonValidarVestimentaSeguridad()
        {
            if (refCheckCorrectTogglesVestimentaSeguridad.VerifyGroupToggles("VestimentaSeguridad"))
            {
                avatarVestido = true;
                refPanelInterfazSoloAnimacionVestimentaSeguridad.Mostrar(false);
                Debug.Log("OnButtonValidarVestimentaSeguridad");
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextProtecionNoAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
        }

        public void OnButtonValidarHerramienta()
        {
            DesactivarHerramientas();

            switch (zonaActual)
            {
                case ZonaActual.Null:
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoPuedeUsarHerramientaAqui"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    break;

                case ZonaActual.VestirAvatar:
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoPuedeUsarHerramientaAqui"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    break;

                case ZonaActual.ZonaSeguridad:

                    break;

                case ZonaActual.Afuera:

                    //check normal
                    if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("DescolgarCortaCircuitos"))
                    {
                        refActiveZonesController.InteractuableZone("TransformadorPertiga", true);
                        refActiveZonesController.ActiveZone("TransformadorPertiga");
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        refActiveZonesController.InteractuableZone("MedicionTension", true);
                        refActiveZonesController.ActiveSpecificZone("MedicionTension", new[] {0, tramoSenializado + 1});
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("pinzaMedidora"))
                    {
                        if (!implementosAlturaColocados)
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSinElementosAltura"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            return;
                        }

                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        refActiveZonesController.InteractuableZone("TransformadorPinza", true);
                        refActiveZonesController.ActiveZone("TransformadorPinza");
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("CambiarCable"))
                    {
                        if (!implementosAlturaColocados)
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSinElementosAltura"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            return;
                        }

                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        refActiveZonesController.InteractuableZone("CambiarCable", true);
                        refActiveZonesController.ActiveSpecificZone("CambiarCable", new[] {tramoSenializado});
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PertigaSola"))
                    {
                        if (refControladorPerdigaConPinzas3.PuestaTierraColocada)
                        {
                            pertigaPinzas.SetActive(true);
                            quitarPinzasZonaActiva.SetActive(true);
                            refActiveZonesController.InteractuableZone("PuestaTierra", true);
                            refActiveZonesController.ActiveSpecificZone("PuestaTierra", new[] {0});
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        }
                        else if (etapaHerramientasCortaCircuitos == 2)
                        {
                            animatorCortaCircuitos.enabled = false;
                            zonaActivaQuitarLetreroNoOperar.SetActive(true);
                            pertigaQuitarLetrero.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                            refActiveZonesController.InteractuableZone("TransformadorPertiga", true);
                            refActiveZonesController.ActiveZone("TransformadorPertiga");
                        }
                        else if (etapaHerramientasCortaCircuitos == 3 || etapaHerramientasCortaCircuitos == 1)
                        {
                            animatorCortaCircuitos.enabled = false;
                            zonasActivasColgarFusibles.SetActive(true);
                            pertigaColgarFusibles.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                            refActiveZonesController.InteractuableZone("TransformadorPertiga", true);
                            refActiveZonesController.ActiveZone("TransformadorPertiga");
                        }
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PonerPuestaTierra"))
                    {
                        refActiveZonesController.InteractuableZone("PuestaTierra", true);
                        refActiveZonesController.ActiveSpecificZone("PuestaTierra", new[] {0});
                        ponerPinzasZonaActiva.SetActive(true);
                        pertigaPinzas.SetActive(true);
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PonerLetreroPeligroCortaCircuitos"))
                    {
                        if (etapaHerramientasCortaCircuitos == 1)
                        {
                            animatorCortaCircuitos.enabled = false;
                            pertigaMensajeNoOperar.SetActive(true);
                            pertigaDescolgarFusibles.SetActive(false);
                            zonaActivaLetreroNoOperar.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                            refActiveZonesController.InteractuableZone("TransformadorPertiga", true);
                            refActiveZonesController.ActiveZone("TransformadorPertiga");
                        }
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    
                    break;

                case ZonaActual.CortaCircuitos:

                    if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        DesactivarHerramientas();
                        puntaDetectoraTension.SetActive(true);
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                    }
                    else if (etapaHerramientasCortaCircuitos == 0)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 0");

                        if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("DescolgarCortaCircuitos"))
                        {
                            if (zonaSeguridadColocada)
                            {
                                animatorCortaCircuitos.enabled = false;
                                zonasActivasDescolgarFusibles.SetActive(true);
                                pertigaDescolgarFusibles.SetActive(true);
                                refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);

                                //resetear todos los objetos

                                countTriggerAnimationColgarCortaCircuitos = 0;
                                countTriggerAnimationDescolgarCortaCircuitos = 0;

                                for (int i = 0; i < zonasActivasDescolgarFusibles.transform.childCount; i++)
                                    zonasActivasDescolgarFusibles.transform.GetChild(i).gameObject.SetActive(true);

                                for (int i = 0; i < zonasActivasColgarFusibles.transform.childCount; i++)
                                    zonasActivasColgarFusibles.transform.GetChild(i).gameObject.SetActive(true);
                            }
                            else
                                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeñalizacion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        }
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaSeleccionadaDesenclavarFusibles"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasCortaCircuitos == 1)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 1");

                        if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PertigaSola"))
                        {
                            if (refControladorPerdigaConPinzas3.PuestaTierraColocada)
                                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajePuestaTierraConectada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            else
                            {
                                animatorCortaCircuitos.enabled = false;
                                zonasActivasColgarFusibles.SetActive(true);
                                pertigaColgarFusibles.SetActive(true);
                                etapaHerramientasCortaCircuitos = 0;
                                refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                            }
                        }
                        else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PonerLetreroPeligroCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            pertigaMensajeNoOperar.SetActive(true);
                            pertigaDescolgarFusibles.SetActive(false);
                            zonaActivaLetreroNoOperar.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        }
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSeleccionarLetreroYPertiga"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasCortaCircuitos == 2)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 2");

                        if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PertigaSola"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            zonaActivaQuitarLetreroNoOperar.SetActive(true);
                            pertigaQuitarLetrero.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        }
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeQuitarLetrero"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasCortaCircuitos == 3)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 3");

                        if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PonerLetreroPeligroCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            pertigaMensajeNoOperar.SetActive(true);
                            pertigaDescolgarFusibles.SetActive(false);
                            zonaActivaLetreroNoOperar.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        }
                        else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PertigaSola"))
                        {
                            if (refControladorPerdigaConPinzas3.PuestaTierraColocada)
                                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajePuestaTierraConectada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            else
                            {
                                animatorCortaCircuitos.enabled = false;
                                zonasActivasColgarFusibles.SetActive(true);
                                pertigaColgarFusibles.SetActive(true);
                                etapaHerramientasCortaCircuitos = 0;
                                refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                            }
                        }
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeColgarDeNuevoCortacuitos"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }

                    break;

                case ZonaActual.Transformador:

                    if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        DesactivarHerramientas();
                        puntaDetectoraTension.SetActive(true);
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PinzaAmperimetrica"))
                    {
                        pinzaAmperimetro.SetActive(true);
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaNoSeleccionada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case ZonaActual.PuestaTierra:

                    if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PonerPuestaTierra"))
                    {
                        if (!refControladorPerdigaConPinzas3.PuestaTierraColocada)
                        {
                            ponerPinzasZonaActiva.SetActive(true);
                            pertigaPinzas.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        }
                        else 
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaSeleccionadaSinFuncion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PertigaSola"))
                    {
                        if (refControladorPerdigaConPinzas3.PuestaTierraColocada)
                        {
                            pertigaPinzas.SetActive(true);
                            quitarPinzasZonaActiva.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        }
                        else 
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaSeleccionadaSinFuncion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaSeleccionadaSinFuncion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;
            }

            refCheckCorrectTogglesHerramientasAlturas.DesactivateAllToggles();
        }

        public void OnButtonValidarImplementosAlturas()
        {
            if (refCheckCorrectToggleSenializacionSeguridad.VerifyGroupToggles("SenializacionSeguridad"))
            {
                if (avatarVestido)
                {
                    refCheckCorrectToggleSenializacionSeguridad.DesactivateAllToggles();
                    refPanelInterfazSoloAnimacionSenializacionSeguridad.Mostrar(false);
                    refActiveZonesController.InteractuableZone("ZonasSeguridadInicial", true);
                    refActiveZonesController.ActiveZone("ZonasSeguridadInicial");
                    canvasGroupBotonesDerecha.interactable = false;
                    canvasGroupBotonesIzquierda.interactable = false;
                    zonaSeguridadColocada = true;
                    return;
                }

                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoProteccionPersonal"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }

            if (refCheckCorrectToggleSenializacionSeguridad.VerifyGroupToggles("ImplementosTrabajoAlturas")) //despues si puedo evaluar el colocar los implementos de altura
            {
                if (zonaSeguridadColocada)
                {
                    refPanelInterfazSoloAnimacionSenializacionSeguridad.Mostrar(false);
                    implementosAlturaColocados = true;
                }
                else
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajePrimeroSenalizarTramo"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSinElementosAltura"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
        }

        public void DesactivarHerramientas()
        {
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
            pertigaColgarFusibles.SetActive(false);
            pertigaDescolgarFusibles.SetActive(false);
        }

        //get values
        public float GetCorrienteCablesTransformadorSituacion3(int argIndex)
        {
            return refTablet.CorreinteDeCablesPorTramo(tramoSeleccionado)[argIndex];
        }

        public float GetTensionLineaPuntasPinzaSituacion3(int argIndex)
        {
            return refTablet.tensioLineaALinea(tramoSeleccionado)[argIndex];
        }

        public float GetTensionFasePuntasPinzaSituacion3(int argIndex)
        {
            return refTablet.tensioLineaAFase(tramoSeleccionado)[argIndex];
        }

        //set Zones
        public void SetEtapaVestirAvatar()
        {
            Debug.Log("SetEtapaVestirAvatar");
            zonaActual = ZonaActual.VestirAvatar;
        }

        public void SetEtapaZonaSeguridad()
        {
            Debug.Log("SetEtapaZonaSeguridad");
            zonaActual = ZonaActual.ZonaSeguridad;
        }

        public void SetEtapaVisualizacionCortaCircuito()
        {
            Debug.Log("SetEtapaVisualizacionCortaCircuito");
            zonaActual = ZonaActual.CortaCircuitos;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaCortaCircutosMalo()
        {
            Debug.Log("SetEtapaCortaCircuto");
            zonaActual = ZonaActual.CortaCircuitos;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaTransformador()
        {
            Debug.Log("SetEtapaTransformador");
            zonaActual = ZonaActual.Transformador;
        }

        public void SetEtapaPuestaTierra()
        {
            Debug.Log("SetEtapaPuestaTierra");
            zonaActual = ZonaActual.PuestaTierra;
        }

        public void SetEtapaAfuera()
        {
            Debug.Log("SetEtapaAfuera");
            zonaActual = ZonaActual.Afuera;
        }

        public void EjecutarAnimacionDescolgarCortaFusibles()
        {
            countTriggerAnimationDescolgarCortaCircuitos++;

            if (countTriggerAnimationDescolgarCortaCircuitos == 3)
            {
                etapaHerramientasCortaCircuitos = 1;
                cortaCircuitosAbiertos = true;
                animatorCortaCircuitos.enabled = true;
                animatorCortaCircuitos.runtimeAnimatorController = animatorControllerDescolgarCortaCircuitos;
                animatorCortaCircuitos.SetBool("Descolgar", true);
                pertigaDescolgarFusibles.SetActive(false);
                zonasActivasDescolgarFusibles.SetActive(false);
                pertigaDescolgarFusibles.SetActive(false);
                ActivarTension(false);
                refPanelAnimationRompeCargasMostrar.Mostrar();
                refExecuteAnimationRompeCargas.PlayAnimacion("Descolgar", () => refPanelAnimationRompeCargasMostrar.Mostrar(false), 1);
                StartCoroutine(CouDesactivarAnimatorDescolgarFusibles());
            }
        }

        public void EjecutarAnimacionColgarCortaFusibles()
        {
            countTriggerAnimationColgarCortaCircuitos++;

            if (countTriggerAnimationColgarCortaCircuitos == 3)
            {
                etapaHerramientasCortaCircuitos = 0;
                cortaCircuitosAbiertos = false;
                animatorCortaCircuitos.enabled = true;
                animatorCortaCircuitos.runtimeAnimatorController = animatorControllerColgarCortaCircuitos;
                animatorCortaCircuitos.SetBool("Colgar", true);
                pertigaColgarFusibles.SetActive(false);
                zonasActivasColgarFusibles.SetActive(false);
                ActivarTension(true);
                StartCoroutine(CouDesactivarAnimatorColgarFusibles());
            }
        }

        public void ColocarLetreroNoOperarCortaCircuitos()
        {
            Debug.Log("SetLetreroNoOperarCortaCircuitos");
            pertigaMensajeNoOperar.SetActive(false);
            pertigaMensajeNoOperar.GetComponent<FollowMouse>().ResetPosition();
            letreroNoOperar.SetActive(true);
            etapaHerramientasCortaCircuitos = 2;
        }

        public void QuitarLetreroNoOperarCortaCircuitos()
        {
            Debug.Log("SetLetreroNoOperarCortaCircuitos");
            pertigaQuitarLetrero.SetActive(false);
            pertigaQuitarLetrero.GetComponent<FollowMouse>().ResetPosition();
            letreroNoOperar.SetActive(false);
            etapaHerramientasCortaCircuitos = 3;
        }

        public void ActivarTension(bool argActivar)
        {
            if (argActivar)
            {
                refControladorTensionCortaCircuitos.ActivarTensionInferior();
                refControladorTensionTransformador.ActivarTension();
            }
            else
            {
                refControladorTensionCortaCircuitos.DesactivarTensionInferior();
                refControladorTensionTransformador.DesactivarTension();
            }
        }

        private void ActivarTramoSenializacion(int argIndexTramo)
        {
            for (int i = 0; i < arrayTramosSenializacion.Length; i++)
                arrayTramosSenializacion[i].SetActive(i == argIndexTramo);
        }

        #endregion

        #region courutines

        private IEnumerator CouDesactivarAnimatorDescolgarFusibles()
        {
            yield return new WaitForSeconds(3);
            animatorCortaCircuitos.enabled = false;
        }

        private IEnumerator CouDesactivarAnimatorColgarFusibles()
        {
            yield return new WaitForSeconds(3.25f);
            animatorCortaCircuitos.enabled = false;
        }

        #endregion
    }

    [Serializable]
    public class Transformador
    {
        public float potenciaKVA;

        public Sprite spriteTransformador;
    }

    public enum ZonaActual
    {
        Null,
        VestirAvatar,
        ZonaSeguridad,
        CortaCircuitos,
        Transformador,
        PuestaTierra,
        Afuera
    }
}