﻿using NSBoxMessage;
using NSCreacionPDF;
using NSEvaluacion;
using NSInterfazAvanzada;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;

namespace NSInterfaz
{
    public class clsRegistroDatosSituacion3 : AbstractSingletonPanelUIAnimation<clsRegistroDatosSituacion3>
    {

        #region members
        
        [SerializeField] private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField] private PanelInterfazEvaluacion refPanelInterfazEvaluacion;

        [SerializeField] private ControladorValoresPDF refControladorValoresPDF;

        [SerializeField] private Evaluacion refEvaluacion;

        [SerializeField, Header("Corriente transformador")]
        private TMP_InputField[] inputCorrienteTransformador;


        [SerializeField, Header("Tension transformador")]
        private TMP_InputField[] inputTensionTransformador;


        [SerializeField, Header("Corriente tramos")]
        private TMP_InputField[] inputCorrienteAntesDelCambio;
        [SerializeField]
        private TMP_InputField[] inputCorrrienteDespuesDelCambio;

        [SerializeField, Header("Tension tramos")]
        private TMP_InputField[] inputTensionAntesDelCambio;

        [SerializeField]
        private TMP_InputField[] inputTensionDespuesDelCambio;



        [SerializeField, Header("%Regulacion Final")]
        private TMP_InputField[] inputRegulacionFinal;

        private float valorPorCampos = 1f / 17f;

        /// <summary>
        /// se llama a la funsion que retorne este vercoto en la situacion
        /// </summary>
        private float[] DatosTensionAntes = new float[4];

        /// <summary>
        /// se llama a la funsion que retorne este vercoto en la situacion
        /// </summary>
        private float[] DatosTensionDespues = new float[4];


        /// <summary>
        /// se llama a la funsion que retorne este vercoto en al situacion
        /// </summary>
        private float[] DatosCorrienteAntes = new float[4];

        /// <summary>
        /// se llama a la funsion que retorne este vercoto en al situacion
        /// </summary>
        private float[] DatosCorrienteDespues = new float[4];


        private float[] DatosinputPruebaPotencia = new float[5];

        private float[] DatosCorrienteTransformador = new float[3];

        private float[] DatosTensionTransformador = new float[3];

        //calificacion
        private float calificacion;

        #endregion

        public void OnButtonValidar()
        {
            if (ValidarEmptyInputs())
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }

            if (ValidarDatos())
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValidarFelicitaciones"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            else
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValidarIncorrectos"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                refControladorDatosSesion.AddIntentos();
            }
        }

        public void OnButtonReporte()
        {
            if (ValidarDatos())
                BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeValidarCorrectos"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextReporte"), OnDatosCorrectos);
            else
            {
                BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeValidarIncorrectosReporte"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextReporte"), OnDatosCorrectos);
                refControladorDatosSesion.AddIntentos();
            }
        }

        public void SetDatosTensionAntes(float argTramoP3P4, float argTramoP5P4, float argTramoP9P10, float argTramoP12P13)
        {
            DatosTensionAntes[0] = argTramoP3P4;
            DatosTensionAntes[1] = argTramoP5P4;
            DatosTensionAntes[2] = argTramoP9P10;
            DatosTensionAntes[3] = argTramoP12P13;
        }

        public void SetDatosTensionDespues(float argTramoP3P4, float argTramoP5P4, float argTramoP9P10, float argTramoP12P13)
        {
            DatosTensionDespues[0] = argTramoP3P4;
            DatosTensionDespues[1] = argTramoP5P4;
            DatosTensionDespues[2] = argTramoP9P10;
            DatosTensionDespues[3] = argTramoP12P13;
        }

        public void SetDatosCorrienteAntes(float argTramoP3P4, float argTramoP5P4, float argTramoP9P10, float argTramoP12P13)
        {
            DatosCorrienteAntes[0] = argTramoP3P4;
            DatosCorrienteAntes[1] = argTramoP5P4;
            DatosCorrienteAntes[2] = argTramoP9P10;
            DatosCorrienteAntes[3] = argTramoP12P13;
        }

        public void SetDatosCorrienteDespues(float argTramoP3P4, float argTramoP5P4, float argTramoP9P10, float argTramoP12P13)
        {
            DatosCorrienteDespues[0] = argTramoP3P4;
            DatosCorrienteDespues[1] = argTramoP5P4;
            DatosCorrienteDespues[2] = argTramoP9P10;
            DatosCorrienteDespues[3] = argTramoP12P13;
        }

        public void SetDatosinputRegulacion(float argTramoP3P4, float argTramoP5P4, float argTramoP9P10, float argTramoP12P13)
        {
            DatosinputPruebaPotencia[0] = argTramoP3P4;
            DatosinputPruebaPotencia[1] = argTramoP5P4;
            DatosinputPruebaPotencia[2] = argTramoP9P10;
            DatosinputPruebaPotencia[3] = argTramoP9P10;
            DatosinputPruebaPotencia[4] = argTramoP12P13;
        }

        public void SetDatosTransformadorCorriente(float argCorrienteX1, float argCorrienteX2, float argCorrienteX3)
        {
            DatosCorrienteTransformador[0] = argCorrienteX1;
            DatosCorrienteTransformador[1] = argCorrienteX2;
            DatosCorrienteTransformador[2] = argCorrienteX3;
        }

        public void SetDatosTransformadorTension(float argTensionX1, float argTensionX2, float argTensionX3)
        {
            DatosTensionTransformador[0] = argTensionX1;
            DatosTensionTransformador[1] = argTensionX2;
            DatosTensionTransformador[2] = argTensionX3;
        }

        private void OnDatosCorrectos()
        {
            Mostrar(false);
            refControladorValoresPDF.SetPanelRegistroDatos();
            refPanelInterfazEvaluacion.Mostrar();
        }

        private bool ValidarDatos()
        {
            calificacion = 0;

            for (int i = 0; i < inputCorrienteAntesDelCambio.Length; i++)
            {
                // correinte antes del cambio del dps
                EvaluarYCompararCampo(inputCorrienteAntesDelCambio[i], DatosCorrienteAntes[i]);

                // correinte Despues del cambio del dps
                EvaluarYCompararCampo(inputCorrrienteDespuesDelCambio[i], DatosCorrienteDespues[i]);

                // Tension antes del cambio del dps
                EvaluarYCompararCampo(inputTensionAntesDelCambio[i], DatosTensionAntes[i]);

                // Tension Despues del cambio del dps
                EvaluarYCompararCampo(inputTensionDespuesDelCambio[i], DatosTensionDespues[i]);

            }
            
            for (int i = 0; i < inputCorrienteTransformador.Length; i++)
            {
                EvaluarYCompararCampo(inputCorrienteTransformador[i],DatosCorrienteAntes[i]);
                EvaluarYCompararCampo(inputTensionTransformador[i],DatosCorrienteAntes[i]);
            }

            for (int i = 0; i < inputRegulacionFinal.Length; i++)
            {
                EvaluarYCompararCampo(inputRegulacionFinal[i], DatosinputPruebaPotencia[i]);
            }

            Debug.Log("este es la calificacion que en el nuevo registro de datos= " + calificacion);
            refEvaluacion.AsignarCalificacionRegistroDatos(calificacion);
            
            if (Mathf.Approximately(calificacion, 1f))
                return true;

            return false;

        }

        
        private void EvaluarYCompararCampo(TMP_InputField argCampoAEvaluar, float argDatoParaComparar)
        {
            float tmpinputCorrienteAntesDelCambio;
            
            // correinte antes del cambio del dps
            float.TryParse(argCampoAEvaluar.text, out tmpinputCorrienteAntesDelCambio);
            var errorCorrienteAntes = Mathf.Abs(1 - (tmpinputCorrienteAntesDelCambio * 1 / argDatoParaComparar)) <= 0.05;
            if (errorCorrienteAntes)
            {
                calificacion = valorPorCampos + calificacion;
                Debug.Log("valor por cada campo= " + valorPorCampos + " valor dado " + calificacion);
                argCampoAEvaluar.transform.GetChild(0).gameObject.SetActive(false);
            }
            else
                argCampoAEvaluar.transform.GetChild(0).gameObject.SetActive(true);
        }


        private bool ValidarEmptyInputs()
        {
            var tmpEmptyInputs = 0;

            for (int i = 0; i < 4; i++)
            {
                if (inputCorrienteAntesDelCambio[i].text.Equals(""))
                    tmpEmptyInputs++;

                if (inputCorrrienteDespuesDelCambio[i].text.Equals(""))
                    tmpEmptyInputs++;

                if (inputTensionAntesDelCambio[i].text.Equals(""))
                    tmpEmptyInputs++;

                if (inputCorrrienteDespuesDelCambio[i].text.Equals(""))
                    tmpEmptyInputs++;
            }

            for (int i = 0; i < 3; i++)
            {
                if (inputTensionTransformador[i].text.Equals(""))
                    tmpEmptyInputs++;

                if (inputCorrienteTransformador[i].text.Equals(""))
                    tmpEmptyInputs++;

            }

            for (int i = 0; i < inputRegulacionFinal.Length; i++)
            {
                if (inputRegulacionFinal[i].text.Equals(""))
                    tmpEmptyInputs++;
            }

            return tmpEmptyInputs > 0;

        }

        private void ResetInputsIncorrectos()
        {
            for (int i = 0; i < 3; i++)
            {
                inputCorrienteAntesDelCambio[i].transform.GetChild(0).gameObject.SetActive(false);
                inputCorrienteAntesDelCambio[i].text = "";

                inputCorrrienteDespuesDelCambio[i].transform.GetChild(0).gameObject.SetActive(false);
                inputCorrrienteDespuesDelCambio[i].text = "";

                inputTensionAntesDelCambio[i].transform.GetChild(0).gameObject.SetActive(false);
                inputTensionAntesDelCambio[i].text = "";

                inputCorrrienteDespuesDelCambio[i].transform.GetChild(0).gameObject.SetActive(false);
                inputCorrrienteDespuesDelCambio[i].text = "";

            }

            for (int i = 0; i < inputRegulacionFinal.Length; i++)
            {
                inputRegulacionFinal[i].transform.GetChild(0).gameObject.SetActive(false);
                inputRegulacionFinal[i].text = "";
            }
        }

        private void SetInputIncorrecto(TMP_InputField argInputField, bool argIncorrecto = true)
        {
            argInputField.transform.Find("ImageBadInput").gameObject.SetActive(argIncorrecto);
        }
    }
}