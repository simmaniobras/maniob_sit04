﻿using NSActiveZones;
using NSBoxMessage;
using NSEvaluacion;
using NSInterfaz;
using NSSituacion1;
using UnityEngine;
using UnityEngine.UI;

namespace NSCharacterWearing
{
    public class CheckCorrectWear : MonoBehaviour
    {
        #region members

        [SerializeField]
        private ControladorSituacion1 refControladorSituacion1;               

        [SerializeField]
        private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField]
        private CheckCorrectTogglesActivate refCheckCorrectTogglesActivate;

        [SerializeField]
        private Button buttonAvatar;

        [SerializeField]
        private ActiveZonesController refActiveZonesController;

        [SerializeField]
        private Evaluacion refEvaluacion;

        private int intentosVestirPersonaje;
        #endregion

        #region public methods

        public void CorrectWear()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Tiene la protección necesaria para continuar.", "ACEPTAR", Close);
            buttonAvatar.interactable = false;
            refControladorSituacion1._avatarVestido = true;
            refEvaluacion.AsignarCalificacionElementosProteccion(1f - Mathf.Clamp(0.1f * intentosVestirPersonaje, -1f, 0f));

            if (refControladorSituacion1._zonaSeguridadColocada)
                refActiveZonesController.DesactivateZone("senializacionSeguridad");
        }
               
        public void IncorrectWear()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Los elementos de protección son insuficientes para realizar la maniobra", "ACEPTAR");
            intentosVestirPersonaje++;
            refControladorDatosSesion.AddIntentos();
        }

        public void Close()
        {
            PanelInterfazWearAvatar._instance.Mostrar(false);
        }

        public void OnButtonVerifyWearSituation1()
        {
            refCheckCorrectTogglesActivate.VerifyGroupToggles("situacion1");
        }
        
        public void OnButtonVerifyWearSituation2()
        {
            refCheckCorrectTogglesActivate.VerifyGroupToggles("situacion2");
        }

        public void ResetWearAvatar()
        {
            refCheckCorrectTogglesActivate.DesactivateAllToggles();
            refCheckCorrectTogglesActivate.ActiveGroupToggles("InitWear");
        }
        #endregion
    }
}
